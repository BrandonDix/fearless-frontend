import Nav from './Nav';
import MainPage from './MainPage';
import AttendConferenceForm from './AttendConferenceForm';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import {Routes} from "react-router-dom";
import {BrowserRouter} from "react-router-dom";
import {Route} from "react-router-dom";
import PresentationForm from './PresentationForm';
 


function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
          <Routes>
            <Route path="mainpage/" index element={<MainPage />} />
            <Route path="conferences/new" element={<ConferenceForm />} />
            <Route path="attendees/new" element={<AttendConferenceForm />} />
            <Route path="locations/new" element={<LocationForm />} />
            <Route path="attendees" element={<AttendeesList attendees={props.attendees} />} />
            <Route path="presentation/new" element={<PresentationForm />} />
          </Routes>
    </BrowserRouter>
  );
}

export default App;
