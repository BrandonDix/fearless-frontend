import React from 'react';

class ConferenceForm extends React.Component {
    async componentDidMount() {
        const url = 'http://localhost:8000/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({ locations: data.locations });
        }
    }
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            starts: '',
            ends: '',
            description: '',
            maxPresentations: '',
            maxAttendees: '',
            locations: []
        };
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleStartsChange = this.handleStartsChange.bind(this);
        this.handleEndsChange = this.handleEndsChange.bind(this);
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
        this.handleMaxPresentationsChange = this.handleMaxPresentationsChange.bind(this);
        this.handleMaxAttendeesChange = this.handleMaxAttendeesChange.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleNameChange(event) {
        const value = event.target.value;
        this.setState({ name: value })
    }
    handleStartsChange(event) {
        const value = event.target.value;
        this.setState({ starts: value })
    }
    handleEndsChange(event) {
        const value = event.target.value;
        this.setState({ ends: value })
    }
    handleDescriptionChange(event) {
        const value = event.target.value;
        this.setState({ description: value })
    }
    handleMaxPresentationsChange(event) {
        const value = event.target.value;
        this.setState({ maxPresentations: value })
    }
    handleMaxAttendeesChange(event) {
        const value = event.target.value;
        this.setState({ maxAttendees: value })
    }
    handleLocationChange(event) {
        const value = event.target.value;
        this.setState({ location: value })
    }
    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        data.max_presentations = data.maxPresentations;
        data.max_attendees = data.maxAttendees;
        delete data.maxPresentations;
        delete data.maxAttendees;
        delete data.states;
        delete data.locations;

        const locationUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        console.log(data)
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            const cleared = {
                name: '',
                starts: '',
                ends: '',
                description: '',
                maxPresentations: '',
                maxAttendees: '',
                location: []
            };
            this.setState(cleared);
        }
    }

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new location</h1>
                        <form onSubmit={this.handleSubmit} id="create-conference-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleNameChange} value={this.state.name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                                <label htmlFor="name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleStartsChange} value={this.state.starts} placeholder="Starts" required type="date" name="starts" id="starts" className="form-control" />
                                <label htmlFor="room_count">Starts</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleEndsChange} value={this.state.ends} placeholder="Ends" required type="date" name="ends" id="ends" className="form-control" />
                                <label htmlFor="city">Ends</label>
                            </div>
                            <div className="mb-3">
                                <label htmlFor="nameTextarea">Description</label>
                                <textarea onChange={this.handleDescriptionChange} value={this.state.description} className="form-control" name="description" id="description" rows="3"></textarea>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleMaxPresentationsChange} value={this.state.maxPresentations} placeholder="Max-Presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control" />
                                <label htmlFor="name">Max Presentations</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleMaxAttendeesChange} value={this.state.maxAttendees} placeholder="Max-Attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control" />
                                <label htmlFor="name">Max Attendees</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleLocationChange} value={this.state.location} name="location" required id="location" className="form-select">
                                    <option>Choose a Location</option>
                                    {this.state.locations.map(location => {
                                        return (
                                            <option key={location.href} value={location.id}>
                                                {location.name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default ConferenceForm;