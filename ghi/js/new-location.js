window.addEventListener("DOMContentLoaded", async () => {
    console.log("Dom loaded");
    const stateUrl = 'http://localhost:8000/api/states/';
    const response = await fetch(stateUrl);
    if (response.ok) {
        const data = await response.json();

        const selectTag = document.getElementById('state');
        for (let state of data.states) {
            const option = document.createElement('option');
            option.value = state.abbreviation;
            option.innerHTML = state.name;
            selectTag.appendChild(option);
        }
    }
    const formTag = document.getElementById('create-location-form');
    formTag.addEventListener('submit', async (event) => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData.entries()));
        console.log(json);
        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newLocation = await response.json();
            console.log(newLocation)
        }
    });

});
//"We are in complete control of the HTML and JavaScript with separate files for each. 
//The HTML shows up in the browser, someone types some stuff into the form, then submits it. 
//Our JavaScript gets the form by its id, listens for the submit event, 
//uses the form object to make a FormData object, then we use that in our HTTP request."