window.addEventListener("DOMContentLoaded", async () => {
    console.log("Dom loaded");
    const stateUrl = 'http://localhost:8000/api/locations/';
    const response = await fetch(stateUrl);
    // Using Url to populate drop down menu with our data
    if (response.ok) {
        const data = await response.json();

        const selectTag = document.getElementById('location');
        for (let location of data.locations) {
            const option = document.createElement('option');
            option.value = location.id;
            option.innerHTML = location.name;
            selectTag.appendChild(option);
        }
    }
    // Requirments to create form and send back to database
    const formTag = document.getElementById('create-location-form');
    formTag.addEventListener('submit', async (event) => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData.entries()));
        const confUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(confUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newConference = await response.json();
        }
    });

});
