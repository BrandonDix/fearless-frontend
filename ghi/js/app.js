function createCard(name, description, pictureUrl, location, date) {
    return `
    <div class="col-sm-3">
      <div class="card shadow p-3 mb-5 bg-body rounded">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="text-decoration-underline card-title">${location}<h6/>
          <p class="card-text">${description}</p>
          <div class"card-footer border-sucess">${date}</div>
        </div>
      </div>
    </div>
    `;
  }

// function placeHolder(){
//   return `
//   <div class="card" aria-hidden="true">
//   <img src="..." class="card-img-top" alt="...">
//   <div class="card-body">
//     <h5 class="card-title placeholder-glow">
//       <span class="placeholder col-6"></span>
//     </h5>
//     <p class="card-text placeholder-glow">
//       <span class="placeholder col-7"></span>
//       <span class="placeholder col-4"></span>
//       <span class="placeholder col-4"></span>
//       <span class="placeholder col-6"></span>
//       <span class="placeholder col-8"></span>
//     </p>
//     <a href="#" tabindex="-1" class="btn btn-primary disabled placeholder col-6"></a>
//   </div>
// </div>`
// }

window.addEventListener('DOMContentLoaded', async () => {

const url = 'http://localhost:8000/api/conferences/';

try {
    const response = await fetch(url);

    if (!response.ok) {
      var alertPlaceholder = document.getElementById('liveAlertPlaceholder')
      var alertTrigger = document.getElementById('liveAlertBtn')
      
      function alert(message, type) {
        var wrapper = document.createElement('div')
        wrapper.innerHTML = '<div class="alert alert-' + type + ' alert-dismissible" role="alert">' + message + '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>'
      
        alertPlaceholder.append(wrapper)
      }
      
      if (alertTrigger) {
        alertTrigger.addEventListener('click', function () {
          alert('Refresh, wait 5 seconds. If nothing appears, pls come back later!', 'success')
        })
      }
        console.error('got an error in the response')
    } else {
    const data = await response.json();

    for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
            const details = await detailResponse.json();
            const name = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const location = details.conference.location.name
            const start1 = new Date(details.conference.starts)
            const start = start1.getMonth() + "/" + start1.getDate() + "/" + start1.getFullYear()
            const end1 = new Date(details.conference.ends)
            const end = end1.getMonth() + "/" + end1.getDate() + "/" + end1.getFullYear()
            const date = start + " - " + end
            const html = createCard(name, description, pictureUrl, location, date);
            const column = document.querySelector(".row");
            column.innerHTML += html;
        }
    }

    }
} catch (e) {
    console.error('error', e)
}

});

